import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import AddQuote from "./containers/AddQuote/AddQuote";
import QuotesList from "./containers/QuotesList/QuotesList";
import EditQuote from "./containers/EditQuote/EditQuote";

import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <Container>
              <Navbar light expand="md">
                  <NavbarBrand>Quotes Central</NavbarBrand>
                  <NavbarToggler onClick={this.toggle} />
                  <Collapse navbar >
                      <Nav className="ml-auto" navbar>
                          <NavItem>
                              <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                          </NavItem>
                          <NavItem>
                              <NavLink tag={RouterNavLink} to="/add-quotes">Submit new quote</NavLink>
                          </NavItem>
                      </Nav>
                  </Collapse>
              </Navbar>
                <Switch>
                    <Route path='/' exact component={QuotesList}/>
                    <Route path='/add-quotes' exact component={AddQuote}/>
                    <Route path='/quotes/:id/edit' component={EditQuote}/>
                    <Route path='/quotes/:quoteId' component={QuotesList} />
                    <Route render={() => <h1>Page not found</h1>}/>
                </Switch>
          </Container>
      </BrowserRouter>
    );
  }
}

export default App;
