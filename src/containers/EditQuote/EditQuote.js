import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuotesForm from "../../components/QuotesForm/QuotesForm";

import './EditQuote.css';

class EditQuote extends Component {
    state = {
        quotes: null,
    };

    getQuotesUrl = () => {
        const quoteId = this.props.match.params.id;
        return 'quotes/' + quoteId + '.json';
    };

    componentDidMount () {
        axios.get(this.getQuotesUrl()).then(response => {
            this.setState({quotes: response.data})
        })
    }

    editHandler = quote => {
        axios.put(this.getQuotesUrl(), quote).then(() => {
            this.props.history.push('/');
        })
    };

    render() {
        let form = <QuotesForm
            onSubmit={this.editHandler}
            quotes={this.state.quotes}
        />;

        if (!this.state.quotes) {
            form = <div>Loading Data...</div>
        }
        return (
            <Fragment>
                <h4 className="edit-title">Edit a quote</h4>
                {form}
            </Fragment>
        );
    }
}

export default EditQuote;