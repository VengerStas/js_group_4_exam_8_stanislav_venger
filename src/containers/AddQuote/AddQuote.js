import React, {Component, Fragment} from 'react';
import QuotesForm from "../../components/QuotesForm/QuotesForm";
import axios from '../../axios-quotes';

import './AddQuote.css';

class AddQuote extends Component {
    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.push('/');
        })
    };

    render() {
        return (
           <Fragment>
               <h4 className="submit-title">Submit new quote</h4>
               <QuotesForm
                   onSubmit={this.addQuote}
               />
           </Fragment>
        );
    }
}

export default AddQuote;