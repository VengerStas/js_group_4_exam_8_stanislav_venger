export const QUOTES_CATEGORIES = {
    'wars': 'Star Wars',
    'famous': 'Famous people',
    'saying': 'Saying',
    'humour': 'Humour',
    'motivation': 'Motivational'
};