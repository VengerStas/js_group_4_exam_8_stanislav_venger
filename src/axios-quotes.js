import axios from 'axios';

const instance = axios.create ({
   baseURL: 'https://collection-of-quotes-venger.firebaseio.com/'
});

export default instance;